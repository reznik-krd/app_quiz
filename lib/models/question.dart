import 'package:flutter/foundation.dart';

class Question {
  final String title;
  final List<Map> answers;

  Question({required this.title, required this.answers});
}

class QuestionData {
  final _data = [
    Question(
        title: 'Кем хочешь стать?',
        answers: [
          {'answer':'Программистом', },
          {'answer':'Героем', 'isCorrect': 1},
          {'answer':'Крутым перцем', },
          {'answer':'Лучшим', },
        ]
    ),
    Question(
        title: 'Что будешь делать?',
        answers: [
          {'answer':'Гулять', },
          {'answer':'Изучать', 'isCorrect': 1},
          {'answer':'Творить', },
          {'answer':'Чудить', },
        ]
    ),
    Question(
        title: 'Как будешь делать?',
        answers: [
          {'answer':'Хорошо', },
          {'answer':'Великолепно', 'isCorrect': 1},
          {'answer':'Отлично', },
          {'answer':'Плохо', },
        ]
    ),
    Question(
        title: 'А может спать побольше??',
        answers: [
          {'answer':'Согласен', },
          {'answer':'Нет', 'isCorrect': 1},
          {'answer':'Подумаю', },
          {'answer':'Ну ладно', },
        ]
    ),
    Question(
        title: 'А может покушать?',
        answers: [
          {'answer':'С удовольствием', },
          {'answer':'Я на д....', 'isCorrect': 1},
          {'answer':'Ну да', },
          {'answer':'Ну нет', },
        ]
    ),
    Question(
        title: 'Не скучно?',
        answers: [
          {'answer':'Пока нет', },
          {'answer':'Ещё нет', 'isCorrect': 1},
          {'answer':'Весело', },
          {'answer':'Здорово', },
        ]
    ),
    Question(
        title: 'Давай больше учиться и узнавать новое?',
        answers: [
          {'answer':'Поддерживаю', },
          {'answer':'Всеми руками за', 'isCorrect': 1},
          {'answer':'Ну началось', },
          {'answer':'Лучше поспать', },
        ]
    ),
    Question(
        title: 'Какие планы на ближайшую минуту?',
        answers: [
          {'answer':'Спать', },
          {'answer':'Есть', 'isCorrect': 1},
          {'answer':'Учиться', },
          {'answer':'Гулять', },
        ]
    ),
    Question(
        title: 'Понравилась приложуха?',
        answers: [
          {'answer':'Да', },
          {'answer':'Конечно', 'isCorrect': 1},
          {'answer':'Беллисимо', },
          {'answer':'Ну пойдет', },
        ]
    ),
  ];

  List<Question> get questions => [..._data];
}
